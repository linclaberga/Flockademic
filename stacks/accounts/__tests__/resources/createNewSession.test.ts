jest.mock('../../src/dao', () => ({ insertSessionUuid: jest.fn().mockReturnValue(Promise.resolve()) }));
jest.mock('../../../../lib/lambda/signJwt', () => ({
  signJwt: jest.fn().mockReturnValue('Arbitrary JWT'),
}));

import { createNewSession } from '../../src/resources/createNewSession';

const mockContext = {
  body: {},
  database: {} as any,

  headers: {},
  method: 'POST' as 'POST',
  params: [],
  path: '/',
  query: null,
};

it('should return the generated session', () => {
  return expect(createNewSession(mockContext)).resolves.toHaveProperty('session.identifier');
});

it('should return the JWT', () => {
  return expect(createNewSession(mockContext)).resolves.toHaveProperty('jwt');
});

it('should return the refresh token', () => {
  return expect(createNewSession(mockContext)).resolves.toHaveProperty('refreshToken');
});

it('should error when the generated UUID could not be stored in the database', () => {
  const mockInsertSessionUuid = require.requireMock('../../src/dao').insertSessionUuid;
  mockInsertSessionUuid.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  return expect(createNewSession(mockContext)).rejects.toEqual(new Error('Could not save generated UUID'));
});

it('should error when the JWT could not be created', () => {
  const mockSignJwt = require.requireMock('../../../../lib/lambda/signJwt').signJwt;
  mockSignJwt.mockReturnValueOnce(new Error('Arbitrary signing error'));

  return expect(createNewSession(mockContext)).rejects
    .toEqual(new Error('We are currently experiencing issues, please try again later.'));
});
