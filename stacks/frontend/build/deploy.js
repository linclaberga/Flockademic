/**
 * This file arranges the deployment of Git branches from our CI.
 * This process is split into three main functions:
 *
 * - setupS3: This function creates new AWS S3 Buckets if needed. It then uploads the build files to the Bucket, and
 *            configures it to act as a static website. In case we're deploying the `master` branch, it also creates a
 *            `www` bucket that redirects to the top-level one.
 * - setupCloudFront: This function creates a new CloudFront distribution if needed. It then points it to the bucket
 *                    created in the previous step. Once this distribution is deployed, it returns the domain created
 *                    by CloudFront.
 * - setupCloudFlare: This function sets up DNS records at CloudFlare to point to our CloudFront distribution.
 *
 * Initially, we just used S3 for storage and CloudFlare as the DNS server. The latter was primarily chosen because it
 * easily allowed us to enable SSL.
 * Once we added client-side routing, however, we needed all 404 requests to be redirected to our index.html. This is
 * the reason CloudFront was added.
 *
 * It might be a good idea to one day investigate whether CloudFront can and should replace CloudFlare entirely.
 */

const fs = require('fs'),
      path = require('path'),
      glob = require('glob'),
      mime = require('mime'),
      CFClient = require('cloudflare'),
      AWS = require('aws-sdk');

const unsetEnvvars = getUnsetEnvvars(['AWS_REGION', 'CF_EMAIL', 'CF_KEY']);

if(unsetEnvvars.length > 0){
  console.error('Required environment variables not set:', unsetEnvvars);
  process.exit(1);
} else {
  const domain = 'flockademic.com';
  const subdomain = getSubdomain();

  // TODO Actually split this up into separate scripts, and a library of convenience functions
  if(process.argv[2] && process.argv[2] === 'await'){
    awaitCloudFrontDeployment(subdomain, domain);
  } else if(process.argv[2] && process.argv[2] === 'undeploy'){
    teardown(subdomain, domain);
  } else {
    setup(subdomain, domain);
  }
}

/* Function definitions */
function setup(subdomain, domain){
  return setupS3(subdomain, domain)
    .then(() => setupCloudFront(subdomain, domain))
    .then((cloudFrontDomain) => setupCloudFlare(cloudFrontDomain, subdomain, domain));
}

function teardown(subdomain, domain){
  // All of the cleanup functions were written to be able to support cleaning up multiple subdomains.
  // Now that we often want to clean up a single subdomain from with GitLab, it might be time to rewrite them to clean
  // up just a single subdomain.
  // Would be cleaner in any case - we can just wrap the current method in a `Promise.all`.
  // For now, though, we just wrap the one domain in an array:
  const subdomains = [subdomain]
  return cleanupS3(subdomains, domain)
    .then(() => cleanupCloudFront(subdomains, domain))
    .then(() => cleanupCloudFlare(subdomains, domain));
}

function getUnsetEnvvars(requiredEnvvars){
  return requiredEnvvars.filter(envvar => !process.env[envvar]);
}

function getSubdomain(){
   const stage = process.env.CI_COMMIT_REF_SLUG || 'dev';
   return stage === 'master' ? '' : sanitiseSubdomain(stage) + '.';
}

function sanitiseSubdomain(subdomain){
  return subdomain
    // Remove everything that is not a valid subdomain character *or* forward slash
    .replace(/[^0-9a-z\-\.\/]/gi, '')
    // The forward slash is used to indicate the type of branch, e.g. feat/<newFeature>.
    .replace(/\//g, '-')
    .toLowerCase();

}

// AWS S3
function setupS3(subdomain, domain){
  console.log(':: Seting up AWS S3');

  let s3Promises = [];
  const hostingPromise = setupBucket(subdomain, domain)
  .then((s3bucket) => {
    return syncFilesToBucket(s3bucket, path.join(__dirname, '../dist'))
      .then(() => allowWebsiteAccessForBucket(s3bucket))
      .then(() => configureBucketAsWebsite(s3bucket));
  })
  .then(() => console.log(`Successfully set up bucket for ${subdomain + domain}`))
  .catch(e => console.error(`Error setting up bucket for ${subdomain + domain}:`, e));
  s3Promises.push(hostingPromise);

  // If we're setting up the domain root, configure the www subdomain to redirect there
  if(subdomain === ''){
    const wwwPromise = setupBucket('www.', domain)
    .then((wwwS3bucket) => redirectBucket(wwwS3bucket, domain))
    .then(() => console.log(`Successfully set up bucket for www.${domain}`))
    .catch(e => console.error(`Error setting up bucket for www.${domain}:`, e));
    s3Promises.push(wwwPromise);
  }

  return Promise.all(s3Promises);
}

function setupBucket(subdomain, domain){
  console.log(`Setting up bucket for ${subdomain}${domain}.`);

  const bucket = new AWS.S3({
    signatureVersion: 'v4',
    params: {
      Bucket: sanitiseBucketName(subdomain, domain),
    }
  });

  return doesBucketExist(bucket)
    .then(bucketExists => {
      if(bucketExists){
        console.log(`Bucket ${bucket.config.params.Bucket} already exists.`);
        return bucket;
      }

      console.log(`Bucket ${bucket.config.params.Bucket} does not exist yet; creating it now.`);
      return bucket.createBucket()
        .promise()
        .then(() => bucket.waitFor('bucketExists', bucket.config.params).promise())
        .then(() => bucket);
    });
}

function doesBucketExist(bucket){
  console.log(`Checking whether bucket ${bucket.config.params.Bucket} already exists.`);

  return bucket
    .listBuckets()
    .promise()
    .then((result) => {
      return result.Buckets
        .filter(foundBucket => foundBucket.Name === bucket.config.params.Bucket)
        .length === 1
    });
}

function syncFilesToBucket(bucket, pathToFiles){
  console.log(`Uploading files to bucket ${bucket.config.params.Bucket}.`);

  const sourcePath = path.join(pathToFiles, './**/*');

  const globPromise = new Promise((resolve, reject) => {
    const globOptions = {
      dot: true,
      nodir: true,
    };

    glob(sourcePath, globOptions, (err, files) => {
      if(err){
        reject(err);
      } else {
        resolve(files);
      }
    });
  });

  const isReviewApp = (bucketName) => {
    // It's a review app if the bucket name is not a full domain, nor the `dev.` subdomain`
    const levels = bucketName.split('.');
    return levels.length > 2 && !(levels[0] === 'dev' && levels.length == 3);
  };

  const indexCacheDuration = (isReviewApp(bucket.config.params.Bucket)) ? 0 : 5*60;

  const uploadPromise = globPromise.then((files) => {
    const filePromises = files.map(file => {
      const filePath = file.substr(path.normalize(pathToFiles).length + path.sep.length);
      const body = fs.createReadStream(file);
      const params = {
        Key: filePath,
        ACL: 'public-read',
        Body: body,
        // When deploying review apps, we want to avoid caching so new deployments are quickly visible:
        CacheControl: (filePath === 'index.html' || filePath === 'fallback.html') ? `max-age=${indexCacheDuration.toString()}`: undefined,
        ContentType: mime.getType(filePath)
      };

      // Create a new promise for each file that resolves when its upload is complete
      return new Promise((resolve, reject) => {
        bucket
        .upload(params)
        .on('httpUploadProgress', function(evt) {
          if(evt.loaded === evt.total){
            resolve(params);
          }
        })
        .send((err, data) => {
          if(err){
            reject(err);
          }
        });
      });
    });

    return Promise.all(filePromises)
      .then(updatedObjects => {
        const updatedKeys = updatedObjects.map(bucketObject => bucketObject.Key);
        return getBucketObjects(bucket).then(bucketObjects => {
          const objectsToRemove = bucketObjects.filter(
            bucketObject => updatedKeys.indexOf(bucketObject.Key) === -1
          );

          return deleteBucketObjects(bucket, objectsToRemove);
        });
      });
  });

  return uploadPromise;
}

function getBucketObjects(bucket, previousObjects = []){
  console.log(`Looking up existing files in bucket ${bucket.config.params.Bucket}`);

  let params = {};
  if(previousObjects.length > 0){
    params.StartAfter = previousObjects[previousObjects.length - 1].Key;
  }
  return bucket.listObjectsV2(params)
    .promise()
    .then(response => {
      const bucketObjects = response.Contents;
      if(!response.IsTruncated){
        return previousObjects.concat(bucketObjects);
      }
      return getBucketObjects(bucket, bucketObjects);
    });
}

function deleteBucketObjects(bucket, objects){
  console.log(`Removing ${objects.length} redundant file(s) in bucket ${bucket.config.params.Bucket}`);

  objects = objects.map(bucketObject => {
    return { Key: bucketObject.Key }
  });

  // S3 can only handle deleting 1000 objects at a time, so split up the work
  let objectSets = [];
  for(let i = 0; i < objects.length; i = i + 1000){
    objectSets.push(objects.slice(i, i + 1000));
  }

  const deletionPromises = objectSets.map(objectSet => {
    const params = {
      Delete: {
        Objects: objectSet,
      }
    };
    return bucket.deleteObjects(params).promise();
  });

  return Promise.all(deletionPromises);
};

// This function is no longer used; versions take up more space
// (i.e. they cost more), and deleting a bucket also requires deleting old versions of objects
function enableVersioningForBucket(bucket){
  console.log(`Enabling versioning for bucket ${bucket.config.params.Bucket}.`);

  return bucket
    .putBucketVersioning({
      VersioningConfiguration: {
        MFADelete: 'Disabled',
        Status: 'Enabled',
      },
    })
    .promise();
}

function allowWebsiteAccessForBucket(bucket){
  console.log(`Allowing website access for bucket ${bucket.config.params.Bucket}.`);
  // From https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html#root-domain-walkthrough-create-buckets
  const websiteHostingPolicy = JSON.stringify({
    "Version":"2012-10-17",
    "Statement":[{
    "Sid":"Stmt1452249382249",
      "Effect":"Allow",
      "Principal": "*",
        "Action":["s3:*"],
        "Resource":[`arn:aws:s3:::${bucket.config.params.Bucket}/*`
        ]
      }
    ]
  });

  return bucket
    .putBucketPolicy({
      Policy: websiteHostingPolicy,
    })
    .promise();
}

function configureBucketAsWebsite(bucket, indexDocument = 'index.html', errorDocument = '404.html'){
  console.log(`Setting up bucket ${bucket.config.params.Bucket} as website with index document ${indexDocument}.`);

  return bucket
    .putBucketWebsite({
      WebsiteConfiguration: {
        IndexDocument: {
          Suffix: indexDocument,
        },
        ErrorDocument: {
          Key: errorDocument,
        },
      }
    })
    .promise();
}

function redirectBucket(bucket, targetDomain){
  console.log(`Setting up bucket ${bucket.config.params.Bucket} to redirect to ${targetDomain}.`);

  return bucket
    .putBucketWebsite({
      WebsiteConfiguration: {
        RedirectAllRequestsTo: {
          HostName: targetDomain,
          Protocol: 'https',
        },
      }
    })
    .promise();
}

function sanitiseBucketName(subdomain, domain){
  // When not deploying to a subdomain, no additional sanitising is necessary
  if(subdomain === ''){
    return domain;
  }
  return subdomain
    // Remove a trailing . from the subdomain - we'll re-add it later
    .replace(/\.$/, '')
    // S3 bucket names can be at most 63 characters long:
    // https://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html#bucketnamingrules
    // In other words, the domain should not be longer than 32 characters,
    // or we should trim the subdomain based on the domain length rather than at 30 chars.
    .substr(0, 30)
    // Remove potential trailing dashes
    .replace(/-+$/, '')
    // Then append the domain name to the bucket name
    + `.${domain}`;
}

function cleanupS3(subdomains, domain){
  console.log(':: Cleaning up AWS S3');

  const buckets = subdomains.map(subdomain => {
    return new AWS.S3({
      signatureVersion: 'v4',
      params: {
        Bucket: sanitiseBucketName(subdomain, domain),
      }
    });
  });

  const deletionPromises = buckets.map(bucket => deleteBucket(bucket));

  return Promise.all(deletionPromises)
  .then(() => console.log(`Successfully cleaned up buckets for ${domain}`))
  .catch(e => console.error(`Error cleaning up buckets for ${domain}:`, e));
}

function deleteBucket(bucket){
  return getBucketObjects(bucket)
    .then(objects => deleteBucketObjects(bucket, objects))
    .then(() => bucket.deleteBucket().promise());
}

// CloudFront
function setupCloudFront(subdomain, domain){
  // Note that this merely starts up the process of setting up CloudFront.
  // It will take a while before everything is fully deployed.
  // See awaitCloudFrontDeployment to wait for full deployment.
  console.log(':: Setting up CloudFront');

  const endpoint = getEndpoint(subdomain, domain);

  return setupDistribution(endpoint, subdomain, domain)
    .then(distribution => {
      console.log(`Successfully set up CloudFront at domain ${distribution.DomainName}`);
      return distribution.DomainName;
    })
    .catch(e => console.error('Error setting up CloudFront:', e));
}

function ensureDeployment(distribution){
  console.log(`Waiting for the CloudFront distribution ${distribution.Id} to be deployed...`);

  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});

  return cloudfront.waitFor('distributionDeployed', { Id: distribution.Id })
    .promise()
    .then(() => distribution);
}

function setupDistribution(endpoint, subdomain, domain){
  return getDistributionSummaryForEndpoint(endpoint)
    .then(distribution => {
      if(typeof distribution !== 'undefined'){
        console.log(`A CloudFront distribution already exists for ${endpoint}`);
        if(!distribution.Enabled){
          // If the distribution exists but is disabled, enable it
          return toggleDistribution(true, subdomain, domain);
        }
        return distribution;
      }

      return createCloudFrontDistribution(endpoint, subdomain, domain)
        .then(data => data.Distribution);
    });
}

function createCloudFrontDistribution(endpoint, subdomain, domain){
  console.log(`Creating a CloudFront distribution for ${endpoint}`);

  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});

  return cloudfront
    .createDistribution(getDefaultCloudFrontParams(subdomain, domain))
    .promise();
}

function getEndpoint(subdomain, domain){
  const bucketName = sanitiseBucketName(subdomain, domain);
  return `${bucketName}.s3-website.${process.env.AWS_REGION}.amazonaws.com`;
}

function getDefaultCloudFrontParams(subdomain, domain){
  const endpoint = getEndpoint(subdomain, domain);
  const originId = `S3-Website-${endpoint}`;

  return {
    DistributionConfig: {
      CallerReference: Date.now().toString(),
      Comment: '',
      DefaultCacheBehavior: {
        ForwardedValues: {
          Cookies: {
            Forward: 'none',
            WhitelistedNames: {
              Quantity: 0,
            }
          },
          QueryString: false,
          Headers: {
            Quantity: 0,
          },
          QueryStringCacheKeys: {
            Quantity: 0,
          }
        },
        MinTTL: 0,
        TargetOriginId: originId,
        TrustedSigners: {
          Enabled: false,
          Quantity: 0,
        },
        ViewerProtocolPolicy: 'allow-all',
        AllowedMethods: {
          Items: [
            'GET',
            'HEAD',
            'OPTIONS',
          ],
          Quantity: 3,
          CachedMethods: {
            Items: [
              'GET',
              'HEAD',
            ],
            Quantity: 2
          }
        },
        Compress: true,
        LambdaFunctionAssociations: {
          Quantity: 0,
        },
        SmoothStreaming: false
      },
      Enabled: true,
      Origins: {
        Quantity: 1,
        Items: [
          {
            DomainName: endpoint,
            Id: originId,
            CustomHeaders: {
              Quantity: 0,
            },
            CustomOriginConfig: {
              HTTPPort: 80,
              HTTPSPort: 443,
              OriginProtocolPolicy: 'http-only',
            },
          },
        ]
      },
      Aliases: {
        Quantity: 1,
        Items: [
          subdomain + domain,
        ]
      },
      CacheBehaviors: {
        Quantity: 0,
      },
      CustomErrorResponses: {
        Quantity: 1,
        Items: [
          {
            ErrorCode: 404,
            ErrorCachingMinTTL: 300,
            ResponseCode: '200',
            ResponsePagePath: '/fallback.html'
          },
        ]
      },
      DefaultRootObject: '',
      HttpVersion: 'http2',
      IsIPV6Enabled: true,
      Logging: {
        Bucket: '',
        Enabled: false,
        IncludeCookies: false,
        Prefix: ''
      },
      PriceClass: 'PriceClass_100',
      Restrictions: {
        GeoRestriction: {
          Quantity: 0,
          RestrictionType: 'none',
        }
      },
    }
  };
}

function getDistributionSummaryForEndpoint(endpoint){
  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});

  return cloudfront
    .listDistributions()
    .promise()
    .then((data) => {
      const DistributionList = data.DistributionList;
      return DistributionList
        .Items
        .find(distributionSummary => {
          const originAtEndpoint = distributionSummary.Origins.Items.find(({DomainName}) => DomainName === endpoint);
          return typeof originAtEndpoint !== 'undefined';
        });
    });
}

function getDeployedDisabledDistributions(){
  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});

  return cloudfront
    .listDistributions()
    .promise()
    .then((data) => {
      const DistributionList = data.DistributionList;
      return DistributionList
        .Items
        .filter(distributionSummary => !distributionSummary.Enabled && distributionSummary.Status === 'Deployed');
    });
}

function cleanupCloudFront(subdomains, domain){
  // This function disables the given CloudFront distribution,
  // and removes all previously disabled distributions.
  // This because it will take a while before a distribution is completely
  // disabled, so we don't want to stall cleanup jobs waiting for that
  // before we can actually delete it.
  // Unfortunate, but that's how it is.
  console.log(`:: Cleaning up CloudFront`);

  const disablePromises = subdomains.map(subdomain => toggleDistribution(false, subdomain, domain));

  return Promise.all(disablePromises)
    .then(getDeployedDisabledDistributions)
    .then(removeDistributions)
    .then(() => console.log(`Successfully cleaned up CloudFront distributions for ${domain}`))
    .catch(e => console.error(`Error cleaning up CloudFront distributions for ${domain}:`, e));
}

function awaitCloudFrontDeployment(subdomain, domain){
  const endpoint = getEndpoint(subdomain, domain);

  return getDistributionSummaryForEndpoint(endpoint)
    .then(distributionSummary => ensureDeployment({ Id: distributionSummary.Id }));
}

function toggleDistribution(shouldEnable = true, subdomain, domain){
  const endpoint = getEndpoint(subdomain, domain);
  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});

  return getDistributionSummaryForEndpoint(endpoint)
    .then(distributionSummary => {
      return cloudfront.getDistributionConfig({ Id: distributionSummary.Id })
        .promise()
        .then(data => [data, distributionSummary.Id]);
    })
    .then(([data, distributionId]) => {
      let distributionConfig = data.DistributionConfig;

      if(distributionConfig.Enabled === shouldEnable){
        // If this distribution was already configured as desired, we don't need to do anything
        return;
      }

      distributionConfig.Enabled = shouldEnable;

      const params = {
        DistributionConfig: distributionConfig,
        Id: distributionId,
        IfMatch: data.ETag
      };
      console.log(
        'Toggling', shouldEnable ? 'on' : 'off',
        'distribution', params.Id,
        'with ETag', params.IfMatch);

      // Make sure no deployment is currently in progress
      return ensureDeployment({ Id: distributionId })
        .then(() => {
          return cloudfront
            .updateDistribution(params).promise()
            .then(distributionData => distributionData.Distribution);
        });
    });
}

function removeDistributions(distributions){
  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});
  console.log('Removing', distributions.length, 'distributions:', distributions.map(distribution => distribution.Id));

  return Promise.all(distributions.map(distribution => {
    return cloudfront.getDistributionConfig({ Id: distribution.Id }).promise()
      .then(data => {
        return cloudfront.deleteDistribution({ Id: distribution.Id, IfMatch: data.ETag }).promise();
      });
  }));
}

function disableAndRemoveDistribution(subdomain, domain){
  const cloudfront = new AWS.CloudFront({apiVersion: '2016-11-25'});

  return toggleDistribution(false, subdomain, endpoint)
  .then(ensureDeployment)
  .then(distribution => {
    console.log('Distribution disabled');
    return cloudfront.deleteDistribution({ Id: distribution.Id }).promise();
  });
 }

// CloudFlare
function setupCloudFlare(target, subdomain, domain){
  console.log(':: Setting up CloudFlare');

  var client = new CFClient({
    email: process.env.CF_EMAIL,
    key: process.env.CF_KEY,
  });

  let names = [ subdomain + domain ];
  if(subdomain === ''){
    names.push('www.' + domain);
  }

  return getZone(client, domain)
  .then(zone => {
    return Promise.all(names.map(name => setDnsRecord(target, client, zone, name)));
  })
  .then(() => console.log('Successfully set up CloudFlare'))
  .catch(e => console.error('Error setting up CloudFlare:', e));
}

function getZone(client, domain){
  console.log('Fetching zone for', domain);

  return client.browseZones({ name: domain })
  .then(function(paginatedRecords){
    if(paginatedRecords.total !== 1){
      throw `Found more than one zone with domain ${domain}.`;
    }

    return paginatedRecords.result[0];
  });
}

function setDnsRecord(target, client, zone, subdomain){
  console.log('Setting DNS record for', subdomain);

  if (subdomain.substr(0, 4) === 'www.') {
    target = `${subdomain}.s3-website.${process.env.AWS_REGION}.amazonaws.com`;
  }

  return getDnsRecord(client, zone, subdomain)
    .then(dnsRecord => {
      if(dnsRecord === null){
        return createDnsRecord(target, client, zone, subdomain);
      } else {
        return updateDnsRecord(target, client, dnsRecord, subdomain);
      }
    });
}

function getDnsRecord(client, zone, subdomain){
  console.log('Fetching DNS record for', subdomain);

  return client.browseDNS(zone.id, { name: subdomain, type: 'CNAME', })
  .then(paginatedRecords => {
    if(paginatedRecords.total === 0){
      return null;
    }

    if(paginatedRecords.total !== 1){
      throw `Found more than one DNS record with subdomain ${subdomain} in zone ${zone.name}.`;
    }

    return paginatedRecords.result[0];
  });
}

function createDnsRecord(target, client, zone, subdomain){
  console.log('Creating new DNS record for', subdomain);

  const dnsRecord = CFClient.DNSRecord.create({
    type: 'CNAME',
    name: subdomain,
    content: target,
    proxied: true,
    zone_id: zone.id,
  });
  return client.addDNS(dnsRecord);
}

function updateDnsRecord(target, client, dnsRecord, subdomain){
  console.log('Updating DNS record for', subdomain);

  if(!CFClient.DNSRecord.is(dnsRecord)){
    throw `Invalid DNS record found for ${subdomain}.`;
  }

  dnsRecord.content = target;
  dnsRecord.proxied = true;
  return client.editDNS(dnsRecord);
}

function cleanupCloudFlare(subdomains, domain){
  console.log(':: Cleaning up CloudFlare');

  let client = new CFClient({
    email: process.env.CF_EMAIL,
    key: process.env.CF_KEY,
  });

  const names = subdomains.map(subdomain => subdomain + domain);

  return getZone(client, domain)
  .then(zone => {
    return Promise.all(names.map(name => removeDnsRecord(client, zone, name)));
  })
  .then(() => console.log('Successfully cleaned up CloudFlare'))
  .catch(e => console.error('Error cleaning up CloudFlare:', e));
}

function removeDnsRecord(client, zone, subdomain){
  console.log('Removing DNS record for', subdomain);

  return getDnsRecord(client, zone, subdomain)
    .then(dnsRecord => client.deleteDNS(dnsRecord));
}
